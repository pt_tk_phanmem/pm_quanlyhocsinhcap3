﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.Model
{
    public class BangDiemModel
    {
        public int ID { get; set; }
        public HocSinhModel HocSinh { get; set; }
        public DiemModel MonToanHoc { get; set; }
        public DiemModel MonLyHoc{ get; set; }
        public DiemModel MonHoaHoc { get; set; }
        public DiemModel MonSinhHoc { get; set; }
        public DiemModel MonTiengAnh { get; set; }
        public DiemModel MonNguVan { get; set; }
        public DiemModel MonCongNghe { get; set; }
        public DiemModel MonGiaoDucVaCongDan { get; set; }
        public DiemModel MonTinHoc { get; set; }
        public DiemModel MonTheDuc { get; set; }
        public DiemModel MonAnNinhQuocPhong { get; set; }
        public DiemModel MonLichSu { get; set; }
        public DiemModel MonDiaLy { get; set; }
    }
}
