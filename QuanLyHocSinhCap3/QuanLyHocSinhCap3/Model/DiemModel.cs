﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.Model
{
    public class DiemModel
    {
        public int ID { get; set; }
        public String DiemMieng { get; set; }
        public string Diem15P { get; set; }
        public String Diem1Tiet { get; set; }
        public float DiemThi { get; set; }
    }
}
