﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.Model
{
    public class HocSinhModel
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public DateTime NgaySinh { get; set; }
        public LopModel Lop { get; set; }
    }
}
