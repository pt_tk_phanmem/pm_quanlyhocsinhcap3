﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.Model
{
    public class LopModel
    {
        public int ID { get; set; }
        public String Ten { get; set; }
        public int SiSo { get; set; }
        public KhoiModel Khoi { get; set; }
        public GiaoVienModel GiaoVien { get; set; }
    }
}
